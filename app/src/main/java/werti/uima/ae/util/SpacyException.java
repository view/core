package werti.uima.ae.util;

/**
 * @author Aleksandar Dimitrov
 * @since 2018-02-10
 */
public class SpacyException extends Exception {
    public SpacyException(Throwable e) {
        super(e);
    }
}
