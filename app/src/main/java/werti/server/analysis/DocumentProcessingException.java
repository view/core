package werti.server.analysis;

/**
 * @author Aleksandar Dimitrov
 * @since 2016-11-12
 */
public class DocumentProcessingException extends Exception {
    DocumentProcessingException(Exception e) {
        super(e);
    }
}
