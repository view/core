package werti.server.analysis;

/**
 * @author Aleksandar Dimitrov
 * @since 2017-01-22
 */
public interface AnnotationStubBuilder {
    AnnotationStub build(int end);
}
