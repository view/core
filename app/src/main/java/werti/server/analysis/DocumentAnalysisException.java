package werti.server.analysis;

/**
 * @author Aleksandar Dimitrov
 * @since 2016-11-12
 */
public class DocumentAnalysisException extends Exception {
    public DocumentAnalysisException(Exception e) {
        super(e);
    }
}
