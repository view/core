package werti.server.interaction.data;

/**
 * @author Aleksandar Dimitrov
 * @since 2017-01-29
 */
public enum AnswerAssessment {
    CORRECT,
    CAPITALIZATION,
    EXTRA_SPACE,
    WORD_CHOICE,
    WORD_FORM,
    AGREEMENT,
    WORD_ORDER,
    STEM,
    SPELLING,
    INCORRECT,
    UNKNOWN
}
