package werti.server.interaction.authentication;

/**
 * @author Aleksandar Dimitrov
 * @since 2017-02-25
 */
public interface AuthenticationData {
    String authenticationToken();
}
