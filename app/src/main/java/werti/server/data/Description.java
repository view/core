package werti.server.data;

/**
 * @author Aleksandar Dimitrov
 * @since 2016-10-15
 */
public interface Description {
    String getTitle();
    String getDescription();
}
