package werti.util;

/**
 * @author Aleksandar Dimitrov
 * @since 2016-10-16
 */
public class DuplicateElementException extends RuntimeException {
    public DuplicateElementException() {
        super();
    }
}
