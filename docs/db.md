# Task

- Task-Id: Unique identifier for this task
- User-Id: The user who started the task
- Website URL: The URL the exercise is based on
- Topic: The topic used in the task
- Activity: The activity used in the task
- Language: The language used in the task
- Filter: The filter used for the activity
- Timestamp: The time the task was started
- NumberOfEnhancements: The individual exercises within the task

# User-Submissions

- User-Interaction-Id
- Task-Id
- Enhancement-Id: The id of the enhancement
- Timestamp: When the user made the submission
- Submission: The text the user submitted
- Correct: was the submission correct?

# Performance

- Performance-Id
- Enhancement-Id
- Task-Id
- Text: The original surface text covered by the exercise
- NumberOfTries: how often did the user attempt to provide the correct answer
- UsedHelpFunction: did the user need the '?'-function
- SolvedCorrectly: Did the user solve the exercise at all?
